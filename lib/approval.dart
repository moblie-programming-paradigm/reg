import 'package:flutter/material.dart';
import 'package:reg/main.dart';

class ApprovalPage extends StatelessWidget {
  const ApprovalPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: const NavigationDrawer(),
      appBar: AppBar(title: const Text('ตารางเรียน')),
      body: SafeArea(
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 1600.0),
                child: ListView(
                  // scrollDirection: Axis.horizontal,
                  children: [
                    Table(
                      defaultColumnWidth: const FlexColumnWidth(),
                      defaultVerticalAlignment:
                      TableCellVerticalAlignment.middle,
                      border: TableBorder.all(width: 1),
                      children: <TableRow>[
                        const TableRow(
                          decoration: BoxDecoration(
                            color: Colors.lightBlueAccent,
                          ),
                          children: <TableCell>[
                            TableCell(
                                child: Text(
                                  '\n วันที่ \n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n รหัสวิชา \n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n ชื่อรายวิชา \n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n หน่วยกิต \n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n กลุ่ม \n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n แบบ \n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n อาจารย์ที่ปรึกษา \n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n อาจารย์ / ผู้สอน	 \n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n คณบดี \n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n ผลการอนุมัติ \n',
                                  textAlign: TextAlign.center,
                                )),
                            TableCell(
                                child: Text(
                                  '\n หมายเหตุ \n',
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                        TableRow(
                          children: <TableCell>[
                            TableCell(
                                child: Container(
                                    color: Colors.white,
                                    child: const Text(
                                      '\n10/2/2566\n',
                                      textAlign: TextAlign.center,
                                    ))),
                            const TableCell(
                                child: Text(
                                  '\n79321163\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nMedicinal\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n2\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n2\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nภาคปกติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nอนุมัติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nอนุมัติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nอนุมัติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nผ่าน\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n-\n',
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                        TableRow(
                          children: <TableCell>[
                            TableCell(
                                child: Container(
                                    color: Colors.white,
                                    child: const Text(
                                      '\n10/2/2566\n',
                                      textAlign: TextAlign.center,
                                    ))),
                            const TableCell(
                                child: Text(
                                  '\n79323163\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nGeneral Knowledge for the Use of Drugs\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n2\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n2\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nภาคปกติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nอนุมัติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nอนุมัติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nอนุมัติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nผ่าน\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n-\n',
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                        TableRow(
                          children: <TableCell>[
                            TableCell(
                                child: Container(
                                    color: Colors.white,
                                    child: const Text(
                                      '\n10/2/2566\n',
                                      textAlign: TextAlign.center,
                                    ))),
                            const TableCell(
                                child: Text(
                                  '\n87639364\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nGeoinformatics in Tourism\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n3\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n1\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nภาคปกติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nอนุมัติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nอนุมัติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nอนุมัติ\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\nผ่าน\n',
                                  textAlign: TextAlign.center,
                                )),
                            const TableCell(
                                child: Text(
                                  '\n-\n',
                                  textAlign: TextAlign.center,
                                )),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

