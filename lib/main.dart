import 'dart:ui';

import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';

import 'approval.dart';
import 'login.dart';
import 'study.dart';

void main() => runApp(const MyApp());

String currentPage = "LoginPage";

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MaterialApp(
        title: 'REG',
        debugShowCheckedModeBanner: false,
        useInheritedMediaQuery: true,
        scrollBehavior: CustomScrollBehavior(),
        builder: DevicePreview.appBuilder,
        locale: DevicePreview.locale(context),
        theme: ThemeData(
          textTheme: const TextTheme(
            bodyText2: TextStyle(fontSize: 18.0),
          ),
          scaffoldBackgroundColor: Colors.white,
          appBarTheme: const AppBarTheme(
            backgroundColor: Colors.amber,
          ),
          drawerTheme: DrawerThemeData(
            backgroundColor: Colors.white,
          ),
        ),
        home: LoginPage(),
      ),
    );
  }
}

class MainPage extends StatelessWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('มหาวิทยาลัยบูรพา')),
      drawer: const NavigationDrawer(),
      body: SafeArea(
        left: false,
        right: false,
        child: ListView(
          children: <Widget>[
            const SizedBox(height: 30.0),
            Image.network(
              'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMSEhMTExMVFRUXGBcVFxUSFRAVFRUVFRUWFhUVFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQFS0dHR0rLS0rLS0tLS0rLSstLS0tLS0tKy0tLS0tLS0tLTctKy0tKzctLTc3LS0tLSs3KysrK//AABEIAMIBBAMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAEAAIDBQYBBwj/xAA4EAACAQMDAgQEAwcDBQAAAAAAAQIDBBEFEiExQQZRYXEiMoHBE5GxFEJyodHh8FKSwgcVIzNi/8QAGgEAAwEBAQEAAAAAAAAAAAAAAQIDAAQFBv/EACIRAQEAAgICAwADAQAAAAAAAAABAhEDIRIxBBNBIjJRYf/aAAwDAQACEQMRAD8Ax/iDU51U9z4z07dclHaW7r1Y048ZfL8orqw3WYuKmmsYa/XBa+E9P2w3NfHUX+2Hl9Tl5NceO3p8PJbjoZG1hFbILEF0Xd+cpPuxtKHPQPurdxbRHCB595LaqrNQordmSTi1h+nnyQLQ3OP4dOWctTgnxltYXxFjfy+FRTim2vm6JJ5k36YG3lSM2tvyx+FbX1yuEu3mWxzy0lljFfY6MsfGm2uGm31QUrWEH8MUv87BllCo1LOGv3cLD75yC3MJITLly3q0+OE10KpRz7Ftayxh+6+mUU9rLhA3iS+lRVFweMuefokDg3eRuT+jXVa0Um2sJd2ZbV/ElNJxgsvzwZ+91yrVSi5fD5Iq5VD1Zjpw+Qi8u5VHmTbBsDc5ONjFtdEhuR2RtgemO3EOSRMwWjLK6dOSlHqsGil4lUsb1z6GTTJIyFGVu9O1WD6P8zTW04Tjh/zPI6dTBb6Vrk6bWXleomWJ/J6HcaZ/pKv9mrRfH5Flo+rwqpYfPkHSaYljKaOoyj8yafqR1dSyi0vKcHyzO6vWpRWU+TSCoq1bdXlhkOqT5RVVLhqpvQrm8lN5HkPhl4tHodSMcybRbS1OC7/oefqrLzGOo+7D4lzy29Io38ZPCl+horJZijxmlcNPhs0+g+K5U8Rn06Z7i+OkW7v4Lcvb7sRT3mv024tSXy/dnAxgnjTRUvjSfVNr2fBmIa5VprEVT8+Y5/nk9b1S1VSDTx3PI9csnSqST4XVfUNwxy9w2Odx9HPxfc+VHy5p/wBxUvEFafLVP6Ra+5R1UPtE20kJ9OH+KTly/wBW8rudRrco8d0muo6rNwjhKK564e5exLRpbYoEvK6fBvrwn4e52nUvENWlHbFU2ueZxbfXPLz15IqviKs1yqf0g/6lbKWP7g1R8G+nC96CcuUntYw8Q1l0/D/2P+pHqWsVLhQVTb8OcbY4+bh559itih2Rpx4zuRPLkyv6fuOM4lyS/hPyHLJahOnZRGpmCkdTOI61wFj8jhseBykbYaJMkREdbNoUyY+LyQRY/cCiOt72cPlfJbUvFdVYzjgzbkcyBt1odR8UVJrCePzyUbuZSeW39QebH26yY0SuJ3A9xJFbtghvEKyOUQuVBr+wPJdBi1GxykdcRjRiIrmu8rl9PN+bEQ3XVe33Yg6Lt9IVI9jDeN9N43pdG/yN3IqNboqVOSa7AM8WmuQi2mo+RzUKGycl5NgAWX/7WmsANYCp1WmFSeULpQJWYPVkPrcEGchAh0YnI8jlEJZO1jZWybRaxpxx0KW3uGkkHU7vKWRLt0Ya0rryKUuAZoO1DD5K3eGI5TtKkcZ3sMkEhNnckTFlgZKpDl6A7mE2q3NB2aTY61snLn+hLVsml0D6PCwcnJYJ+S/hNKWaGNhF01kFk+R4jlPwpBFngFyFWMTVsBtSnx6jIXDXUlqZA5sXej5DVPIFcRwyS2nz6E1zbv3THKr8jeTsoNEe/BiWBbufK9vuxCu+q9vuxBT0+lpRA7ummsBjZBWBaZ5B4otNlWWe/Jn6kDaeOaWKmTIyhz6GNIloWmUmcr8BNOaSXJXajVzwgRTXQK4nlnIoYidU+Bon+o0zsGxzSO7khtMm25We5HvaG0qnIRKKYhpQ9Wp5kCfJNWiRRRgTroRyRIMyYEbRxokYmzCG+gfY1EgaUR9OJtDOltO5wQzuX2IFBsk2pLkXUNc6jmm+SNRYpzGOY/4S0mT2lTDIExsZY5AMXl1jan9AHqL8fekh+OgNHt6TUYFxb4cUu5S06nQtLKXcHpsYGu7XHYprijh5NRfPMSjuUsP/ADoaVslHdPn6CFdR5/z1EM57H01IgmESIKyBT62wvjK2c5xSRRz0BKOe/wDI2WpxzIr6kODlz5bLp0YcfTz27t5QbT/MrKjNvrNinFtdepirmGGy3HnuF5MdIIR5QYugNRXIQkVTQuWRscblueFnl+nfA503njoPjbN8v9Ria7G6tTt90f2X8Rx2pS/E67u7XoQKWFyS0MR/uNqcsU8iKMVJnJUgilRY+pS4Bs1xAYGpE9SA1RCXRijwcUCeMR7p9zDoNs9CWhE5VeOw6jIzaGxgQ3FPzJ6dTDz1AL6o237mgOOUegypTQ2ko4eXz2HwlwEEMVgUmTUoZZDMwpbWWGG5KyJY0JZQtNPaShHkn/bFEVvTIrmhhZwKpY5V1DPoD1ZcAuzkJhHMTeiyK+6hyvb7sRNcvle33YhkbO30fMGuHhE85gN1UEzy8YfCbqruIZeSvrwaLtxi89Cvr0zzbnuu7GajPai8JmJvocv3N9rNH4TB3keWdPx8ickV8Y8jpTOxfP8AI5VR17clhKqP/E4IlEdGjJ9hmmNSRzILo0MCtaOAmUkkLtWQyTSIZVMkVWvyRwrZCW1K4PyGqGApLgZJZCRCkF04oGdMkoVMMWnwqWrb7gGtRcS2pvJyvRymLL2pcdxX058D6tOM08rns/L6AlaG1jYVnkohZoNVg4vHUdBhk6ifVDNsexi+0dJ8kM+oQ0QNcmEsBdrLBBTp5xgudO0Kc0n0RqOPRtGukSVcSLOfhV4+GWX3QTb+HdsJZ+bHDFXw7ZetQWMoitGWdWlhuL6or3T2y9zOjPjknQHUYvf9P6iCtQhmS9l+rEF52Uu3tU9U9CJ19zyZ2V3uYTRvezOHny3HZxcX6ttxHUkNozyOqo4d6dExVt7DKZhdXppSePM391T4MdrlHDOr4+XZM50zFRcos7C0U1yivqllp1xtg0duXpz4/wDRTs4RfREFSaWUgWtfttrsMhXQZK3SaLG3Eske8bVkGFoOrIjpz5JpxyMjBIojfawVXhEVzWaRFGodnDcgsjoXDz1CIyyDxt5LsEKGBaOKytyfHmD2oVLzI/rql6AX1Dd0KmpSabyaSm8vlBNXTYVF2TDM9FvHtk4HVDksLvS5QfTK9mRUqOccD+csT+uw+3t+HJ+XADjllzdfDFdvQrreg5c9Aytljofo9vzlmvspdDN2MMcMvrRgJJtd0ZBDjlANCQfRYFYyHiCz21Ny6PkpKtNPsb3WrPfB+eMoxUo44DHZhfKALu2fw8Z+Ht7sQZWuduF6fdiGcmeH8q0C4JIS5GNDWzy67pNLOxr89Sz3ZXUz1KpyW9tPKI5wdH1TMa/S4yaebKvVKG6LRuLLVJlHn11HkVpPnDDNRt9rKlvaz1cLMo48+qkr02n6ESkw3fvSQLUpOPsPKnZXYVeiCeqBFE0Ogafvi5S+hr002oKkGRpG3noKcXIGfhrMdy6GmcrXFkUyWFRhtWwWWvIkoaY5PEUN5F8aDVZ9CWmi3j4cq8cEsdDmuMci3KGxxA0+B1SfqSanZyo/MVlSp5g1tTelnQkvMlhX29yhdxLsEU6rfX/GC4mmbUU6iqLkGrWqWXgj0t9+wVdy+Eh6qvuKi4pOpJQQZO02LbjkI0OinUbfYV88yZXFz5ew1OOGXFr2KqnIsraXA5VrR6h1MCteSzo0mYdpYw8zHa9Zbajx0fJtNhU+IbbdDd3X6G2vwZ6unneo/Mvb7sRLqsPiX8P3Z0Y2eP8AKtKxjE2NkeWudTZb2EuCoiW1h0JZiLwDVl2YWD1ETlCsxr9lxlGMulyehaxHMX7GAvOJNHp/Gu44uaO2VTDwH/hJlJCfPBZ2l1njudGUJhkZWpbS803WlClsxz5lbW5QEuv2NO4GT0azu99Jc9iRXfwbcc9DN+Hb+MZYk8Jmit7ygm+ePuLpTCzfagr6c8theh0HCos9ApXtOdTbxgt7a0g3lPgC1uI6vKLccfUOdGCxlLJUqjhuSfCKXUNce9f/ACwJZaP/AOoFssRwYj9myaLxNr6r7UljC/mUDy+5SXRDZ01E7bUXN9OMlhZWGeWH0bOK7fkLlmpjx7NtcJY8jtzUQT+EkU2pVscE5N1TO+MWehVeZsiqvlkejvbBy8/6HR3NbulBB9q+iA0wm1lygwq9slyXVt2Kmzj3LmgugQ2IUAa9t1KDi++Q2CFUhkKuNeSa7RcamPJf8pCNH4r0rNbK7xX6yODRS5dg5VV9zkZprKZnbu/cs4LDRajcepw3ism1seTd0tqZc6auCkpsudNfByZqj5EFVEpDWkRndCqTW5Ypt+nQ8/uMtmy8S1+FHzMbXiex8bCzDbg5spboJIfTqYeRYGtHQjLpbUbhNEEocgMJeoZTuU+oNNs5zx0J4V+OSCY3A2m2KVfD6l5OpKlCEo1t29ZaX7voZrJJGQNGmVaF6rU243gE63qCJkrpMBt06lBSZb2lh5kel6dnlmhpWuEQzydPHh+0LCmkdnwFTopFffXUYLqTm66MrJA9/c7VhGbrTcn6kt/fb3/nJBbrMkdGOOo4OTPa7sliKRLk5brCQ7aChElKHIRTXKOU4EsYglar3TXlIu7dFFpHkX9HjuNstghDpS4Imwa8udqfPJjYqjXbiP4iz/p/5SEUetVG5p5/d+7EPC29s1qqpxSjDr0bH+H+5SSqZ5fUvfDtPhshydY9rcV3kuEi505fCVcIlxp8fhPNzdmxXYDvZpLqHMzviKttXubgw8sicmWsWa1W53yb/IrZwyFVBsaeeO7PoZjMcHlZXyyCU7NuLkB1IG+hpeKP0yY3UKO2TXqcs5ZldL3DWKsfBxdSWcSLBWRGiKdVruTwqZA4Mk6GsHY2MM9jqgQU7hh1C5XdAppE1tRbfTqXNKxfDZW071B8NTTIZbXx8R0ZuPQer+XQpal9nIFcXzXRg+van26X17q+0zV9eOb5bIatw5dWD1CmOGkM+XbrmTWlTDQNFj6b5RRJq7WWUTxiV+lyykW0ESsPjUkETRiRRCqMRGWOlvEkXzp9GUFj1NDQqLasmY2pV2xbZQ1q+7IZqF1u4RXKlkeDKqNU+Zc/u/diOaxLE17fdiKQtvbBwfJr9Kio04lBp1g5vLXCfc0UZpJLy4Oblu+nTxYaGRlyWuny4M7Kul1Y7/vahHjl+Ry/RlndRXPkmLT1rqMU22Y/W9QVSXDzgCv9TlUXL8+AFT5PR+N8O4XdcXL8jy9JY9cFho9r+JVSxwuWB0lxnv0RrPDtooQ3Pqy/yuTxwJwYeVWNzBKO30ML4goJTN3NbjHeKKfxZPI4L/Lbt5JrFlZsiaySVHkjPTlcNJRJOwxD4yDonokTQlkYiSDMOxFKDJXki/EGyqG6p5dHuYPVkJtkUpGsa06J1kSkPMV06pYGRY2cgMvNHulna37ZNBTl6r6GGoVMM0FhqSaSlx6sS4mjQRCaUiut6uQq3nkTxOuLJE1/duKwurA6Fbb3K+vcuU85bDoNrG3m5PAZtwgW2gGPp9DNKymuvNRfw/8AKQhuvP8A8i/h+8hDwL7C0lwvYT7+whHLl/Z2X0qdQk/Mhj0+ghHf8dx83opEchCO1yLSHWmbWh8q/wA7CEeT896PxPSZGR8YfL+YhHF8b+y/N6Yx/Y4xCPTjgriOQ7nRFCJ49h5wRNjzjEIykOYPI6IwUxD2IQaEIZIQhWrkAul0QhBGLjSpPPVl3avlCELkK1vfkKxdvcQhWX9l0J5dBCAzKa3/AOxfw/eQhCKT01f/2Q==',
              height: 250,
              width: 250,
            ),
            const SizedBox(height: 30.0),
            const Text('  รหัสนิสิต: 63160205'),
            const SizedBox(height: 5.0),
            const Text('  ชื่อ-สกุล: นางสาวปริยากร แดงกระจ่าง'),
            const SizedBox(height: 5.0),
            const Text('  คณะ: วิทยาการสารสนเทศ '),
            const SizedBox(height: 5.0),
            const Text('  สาขา: วิทยาการคอมพิวเตอร์'),
            const SizedBox(height: 5.0),
            const Text('  สถานภาพ: กำลังศึกษา'),
            const SizedBox(height: 5.0),
            const Text('  อ.ที่ปรึกษา: อาจารย์ภูสิต กุลเกษม,'),
            const SizedBox(height: 5.0),
            const Text('  ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน'),
            const SizedBox(height: 20.0),
          ],
        ),
      ),
    );
  }
}

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      // Add a ListView to the drawer. This ensures the user can scroll
      // through the options in the drawer if there isn't enough vertical
      // space to fit everything.
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: [
          const SizedBox(
            height: 115.0,
            child: DrawerHeader(
              decoration: BoxDecoration(color: Colors.amber),
              child: Text(
                'เมนู',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),

          ListTile(
            title: const Text('ออกจากระบบ'),
            onTap: () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => LoginPage(),
              ));
              currentPage = "LoginPage";
            },
          ),
          ListTile(
            title: const Text('โปรไฟล์'),
            onTap: () {
              if(currentPage == "MainPage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const MainPage(),
                ));
                currentPage = "MainPage";
              }
            },
          ),
          ListTile(
            title: const Text('ตารางเรียน'),
            onTap: () {
              // Update the state of the app
              // ...
              // Then close the drawer
              if(currentPage == "StudyTablePage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const StudyTablePage(),
                ));
                currentPage = "StudyTablePage";
              }
            },
          ),
          ListTile(
            title: const Text('ผลอนุมัติเพิ่ม-ลด'),
            onTap: () {
              if(currentPage == "ApprovalPage"){
                Navigator.pop(context);
              }else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => const ApprovalPage(),
                ));
                currentPage = "ApprovalPage";
              }
            },
          ),
        ],
      ),
    );
  }
}

class CustomScrollBehavior extends MaterialScrollBehavior {
  @override
  Set<PointerDeviceKind> get dragDevices => {
    PointerDeviceKind.touch,
    PointerDeviceKind.mouse,
  };
}

