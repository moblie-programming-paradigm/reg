import 'package:flutter/material.dart';

import 'main.dart';


class LoginPage extends StatelessWidget {
  final TextEditingController username = TextEditingController();
  final TextEditingController password = TextEditingController();


  LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            title: const Text('Burapha University'),
          ),
          body: SafeArea(
            child: Center(
              child: ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 300.0),
                child: Column(
                  children: [
                    const SizedBox(height: 50.0),
                    const Image(image:NetworkImage('https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Buu-logo11.png/1200px-Buu-logo11.png'),
                        height: 200
                    ),
                    const SizedBox(height: 50.0),
                    TextField(
                        controller: username,
                        decoration:
                        const InputDecoration(labelText: 'Username')),
                    TextField(
                        controller: password,
                        obscureText: true,
                        decoration:
                        const InputDecoration(labelText: 'Password')),
                    const SizedBox(height: 30.0),
                    ElevatedButton(onPressed: () {
                      if (username.text.isEmpty ||
                          password.text.isEmpty) {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return AlertDialog(
                                title: const Text("Error"),
                                content: username.text.isEmpty && password.text.isEmpty
                                    ? const Text("กรุณากรอก username และ password")
                                    : username.text.isEmpty
                                    ? const Text("กรุณากรอก username")
                                    : const Text("กรุณากรอก password"),
                                actions: [
                                  TextButton(
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: const Text("ตกลง")),
                                ],
                              );
                            });
                      } else {
                        Navigator.of(context)
                            .pushReplacement(MaterialPageRoute(
                          builder: (context) => const MainPage(),
                        ));
                        currentPage = "MainPage";
                      }
                    },
                        child: const Text('Login')),
                  ],
                ),
              ),
            ),
          ),
        );
  }
}
